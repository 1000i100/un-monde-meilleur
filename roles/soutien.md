# Soutien émotionnel
Vous serez plusieurs à endosser ce rôle nommé selon les milieux : soutien psychologique, émotionnel, empathique ou girafe volante, healer, care-team, psy...

Contrairement aux autres rôles, vous n'allez pas incarner un personnage mais essayer d'incarner au mieux une posture, celle de soutien émotionnel :
vous êtes l'amour de votre prochain, peu importe qui il est ou ce qu'il vous dit, il n'est pas méchant mais en détresse et/ou en souffrance.

En gros, certains participants (joueurs ou soutiens, indépendamment du personnage), quand ils en ressentent le besoin, vont vous appeller à l'aide.
Votre rôle est de les ramasser à la petite cuillère et de les remettre sur pied. La façon de faire est décrite dans votre mission.
Si vous êtes familier avec l'écoute empathique, l'écoute active et/ou la CNV (Communication Non Violente), cela devrait vous faciliter la tâche.

## Votre mission :
- être totalement centré sur votre interlocuteur
- l'écouter avec empathie, quoi qu'il vous dise
- être réceptif/attentif aux émotions de votre interlucteur (exprimées de façon verbale ou gestuelle).
- accueillir ses émotions avec bienveillance
- s'abstenir de tout jugement à son égard
- s'abstenir de donner des conseils
- s'abstenir de donner son avis
- s'abstenir de demander des précisions ou éclaircissements, vous n'avez pas besoin de comprendre ce qui arrive, juste d'accueillir ses émotions
- aller chercher un autre soutien pour vous remplacer si demandé
- ne rien tenter avec votre interlocuteur sans lui demander la permission
- avec toute votre bienveillance, si vous sentez que ce qui suit pourrait soulager votre interlocuteur, vous pouvez lui proposer : (peut-être demandra-t-il directement)
    - répéter les mots clef les plus significatif dans ce que vous avez entendu.
    - reformuler ce que vous avez entendu, plus particulièrement les émotions, toujours sans jugement, injonctions, avis, ni conseils.
    - essayer de deviner les besoins en souffrances chez votre interlocuteur (en les proposant sous forme de question, par exemple : *Est-ce que ce qui t'a été dit te semble injuste ? Tu aimerais plus de justice, d'égalité de traitement ?*)
    - offrir un câlin (en étant prêt à accepter paisiblement que la proposition soit déclinée, pour un câlin comme pour le reste)


Si vous êtes sur place, c'est plus agréable, mais si vous êtes joignable par téléphone, ça peut suffire (dans ce cas, considérez vous d'astreinte durant la durée du cercle ainsi que les heures qui suivent afin que les personnes qui se sentiraient mal puissent compter sur vous en vous contactant).

**Il est important que vous ne participiez pas au cercle, pour ne pas être influencé par ce qui s'y dit.**

Vous n'avez pas besoin de savoir quel personnage incarne votre interlocuteur quand vous lui apportez du soutien.<br>
Si vous le devinez, ce n'est pas grave, sauf si ça vous empèche de rester dans l'empathie et l'écoute bienveillante de cette personne.

**Si vous n'êtes plus en mesure** d'apporter empathie et écoute bienveillante à votre interlocuteur, c'est ok : invitez-le à s'adresser à un autre soutien et allez vous-même chercher de l'écoute auprès d'un 3ème soutien.

En plus de fournir du soutien à la demande en cours de cercle, vous interviendrez en fin de cercle comme indiqué si dessous : 

## Accompagnement de fin de cercle
En fin de partie, tous les participants (vous y compris) seront accompagnés par une des personnes soutiens (simultanément ou à tour de rôle selon les effectifs) dans leur redescente psychologique de l'expérience qu'ils viennent de vivre.

Pour cela, voici les questions que vous poserez à votre interlocuteur :
- Comment te sens-tu ? *En ajoutant si besoin :* Prend le temps de chercher plus loin que "ça va bien".
- Qu'as-tu vécu émotionnellement ces dernières heures et où en es-tu maintenant ?
- Te sens-tu détaché du personnage que tu as incarné et en paix avec le fait de l'avoir incarné ? *Si la réponse est non :* Que se passe-t-il pour toi, qu'est-ce qui coince ? *et revenir à la première question :* Comment te sens-tu ?
- Te sens-tu prêt à retourner à ta vie ou as-tu besoin de plus d'écoute et de soutien ?
- *Et si besoin :* Souhaites-tu que l'on demande à un professionnel de t'accompagner psychologiquement ? *Si l'orga dispose de psy contactables et prêt à intervenir soit bénévolement, soit grâce à une caisse de soutien constituée au préalable pour les rémunérer.*
