# Facilitateur


### Avant cercle spécifique :
Portez-vous un regard, sur vous, un des participants (joueur ou personnage) ou l'acte
qui affaiblit votre capacité à vous concentrer sur l'humanité de chacun ?
qui freine votre bienveillance à l'égard de quelqu'un ?
qui entrave votre empathie ?

Si oui : 
 - Qu'est ce qui est touché chez toi ? Quel sens cela a ? (reformuler au besoin)
 - Que peux-tu faire pour changer ça ?
 - As-tu besoin d'aide, de soutien ? Maintenant ou à un moment spécifique du cercle ?
 - Comment peux-tu faire pour recevoir ce soutien ?
 - As-tu envie de continuer ?


 
 
