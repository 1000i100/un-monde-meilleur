# Dans la peau du système : le comprendre et faire mieux.

Animation inspirée du jeu de rôle et des cercles restauratifs pour comprendre les intrications qui paralysent et précipitent notre monde à sa perte... et explorer comment y remédier.

## Objectifs

- Comprendre intellectuellement et accueillir émotionnellement, avec empathie ce que vivent chaque acteur déterminant de notre système sociétal (du président au sans papiers).
- Identifier la part de marge de manoeuvre de chacun et ce dont il faut avoir conscience pour vouloir s'en servir pour aller vers un monde meilleur, durablement viable, résilient.
- Explorer les actions possibles pour agir efficacement à résoudre les problèmes sociaux, démocratiques, écologiques/climatiques...

En bonus, faire connaître et réutiliser ce format d'animation par qui veut (participants enthousiasmés par exemple) pour toucher un public toujours plus large et rendre chacun acteur d'un monde meilleur.


## Vocabulaire et définitions

- Cercle : c'est ainsi que sera désigné la partie, le jeu, l'animation, l'activité proposée. En effet elle se déroulera essentiellement sous forme de personnes discutant assises en cercle.
- Participant : toutes personnes prennant part à l'activité, en tant que joueur, orga, soutien, hôte ou autre...
- Joueur : personne incarnant un personnage durant le cercle (dont le facilitateur).

## Description & mise en oeuvre

Durée estimée : 4h à 72h selons l'envie des participants et les contraintes des organisateurs.

Nombre de participants minimum : 6 joueurs (+3 [personnes soutiens](roles/soutien.md))
- le facilitateur (meneur de jeu / garant du cadre)
- citoyen indigné
- Nicolas Hulot
- Emmanuel Macron
- un député ou sénateur
- un migrant sans papiers ni domicile

Nombre de participants recommandés : 10 à 30 joueurs (+ 2x le nombre de joueurs en [personnes soutiens](roles/soutien.md))
- le facilitateur (et un co-facilitateur par 10aine suplémentaire de joueurs)
- le personnage apportant sa souffrance par rapport à un fait précis
- les personnages listés comme nécessaires par le personnage apportant sa souffrance
- les personnages listés comme nécessaires par ceux conviés à la ligne précédente...
- et ainsi de suite tant qu'il y a des joueurs pour les incarner.

### Pour l'orga

- choisir le scénario (donc choisir l'initiateur et l'acte précis qui servira de point de départ au cercle)
- selectionner les fiches personages lié au scénario (celle listé par l'intitiateur comme nécessaire pour faire évoluer le conflit autour de l'acte, et ceux que ces derniers sitent comme nécessaire)
- s'assurer de la disponibilité d'un lieu (le réserver pour la durée de l'évènement) pouvant accueillir en un cercle unique autant de personnes que de fiche personnage (+ les personnes soutiens)
- communiquer sur la tenue du cercle pour trouver des joueurs motivés.
- vérifier avec chacun d'eux qu'un rôle leur conviens (qu'ils ont envie de l'incarner et qu'ils sont capable de l'incarner avec empathie, sans voir leur propre personnage comme un "méchant") 

Il est aussi possible de communiquer sur le projet de tenir un cercle, et une fois suffisement de joueurs identifiés, leur demander leur disponibilité et le scénario qui les motive le plus.

Bref, le jour J, vous aurez besoin d'avoir rassemblé :
- Suffisement de joueurs enthousiastes à incarner les personnages nécessaire au scénario
- Suffisement de personnes soutiens
- Un lieu suffisement grand pour accueillir tout ce monde durant la durée du cercle et disponible le jour J.

#### Recommandations complémentaires
- Proposer une prise en charge collective des **enfants** pour permettre au parents de participer au cercle s'ils le souhaitent
- Proposer une prise en charge collective des **repas** par exemple : auberge espagnole avec ingrédients listés sur chaque plat pour que chacun puisse manger quel que soit ses spécificité allimentaire
- Proposer une prise en charge collective des **frais** des orga (location salle, com...) ainsi que des participants (trajets, hébergement...) sous forme de [corresponsabilité financière] par exemple



### Avant de venir

Prévoyez d'apporter à manger sauf si l'orga vous propose une autre option.<br>
Si l'animation est prévue pour plus d'une journée, prévoyez de quoi dormir sur place ou à proximité.<br>
Préparez-vous à être remué émotionnellement et à oser demander du soutien, il y aura du monde spécifiquement pour ça.

#### Avant cercle

Chaque joueur est accompagné 5 à 15 minutes par le facilitateur*
pour se centrer sur son personnage et l'acte qui sert de point de départ au cercle.

* ou co-facilitateur, ou soutien s'il y a trop de joueurs pour laisser ça au facilitateur

Cet accompagnement se déroule grace aux questions suivantes :
- te sens-tu dans ton personnage ?
  - si non :
    - pourquoi ? Qu'est ce qui te freine/bloque pour te sentir à l'aise à incarner ton personnage/répondre oui à la question précédente ?
    - que peux-tu faire pour changer ça ? As-tu besoin d'aide, de soutien pour ça ?
  - si oui, continuer.  
- peux-tu décrire avec tes mots l'acte qui va servir de point de départ au cercle ?
- qu'est ce qui est touché affectivement/émotionnellement chez toi (personnage) en lien avec l'acte ?
- te sens-tu toujours dans ton personnage ? (oui/non comme plus haut)
- te sens-tu capable d'entendre et d'accorder légitimité et empathie aux vécus des autres en lien avec l'acte ? (oui/non comme plus haut)
- Le cercle va bientôt commencer, sont prévu les phases suivantes :
  - Compréhension mutuelle (où chacun en est, quel est son vécu en lien avec l'acte)
  - Auto-responsabilisation (ce qui, chez chacun, à motivé la/les décisions d'agir ou non en lien avec l'acte)
  - Plan d'action (qui veux faire quoi, en tant que personnage... en tant que joueur ?)
  - Retrospective (vos vécus, frustrations, pépites, en tant que participant)
  - Fin de cercle (accompagnement individuel pour retourner à sa vie quotidienne au mieux psychologiquement)  
- prêt ? As-tu envie de poursuivre ? (oui/non comme plus haut)

**Note :** Le facilitateur et les co-facilitateurs ont également un avant cercle, mené par un co-facilitateur
ou une personne soutien en l'absence de co-facilitateur.
Pour eux, on pourra remplacer *ton personnage* par *ton rôle* ou *ta posture de facilitation*.



### Le cercle

// TODO: décrire le déroullement.

### Fin de cercle
Quand tout le monde est OK pour arrêter (ou qu'il n'y a plus de sens à continuer sans les personnes qui ont choisi d'arrêter), le cercle prend fin, soit définitivement, soit le temps de la nuit ou du repas si c'est ce qu'ont convenu les participants (et orga).

Chaque participant qui quitte définitivement le cercle (ou personne soutien qui souhaite arrêter) est invité à prendre un temps avec une personne soutien pour être accompagnée dans sa redescente psychologique et émotionnelle de l'expérience qu'il vient de vivre.
Vous trouverez plus de détails sur le processus d'accompagnement dans la fiche de rôle des Personnes soutiens / Healer : [Accompagnement de fin de cercle](roles/soutien.md#Accompagnement-de-fin-de-cercle)


## Les rôles

L'écriture des rôles est un travail pharaonique, votre soutien est bienvenue !

**Comment écrire un nouveau rôle ?**
1. Lire les rôles déjà disponibles pour se faire une idée de quoi chercher/demander
2. Contacter la personne dont on veut écrire la fiche personnage pour lui demander conseil. Si elle n'est pas joignable ou coopérative, demander à ses proches (familiaux ou professionnels)
s'ils peuvent vous aider à déterminer quoi mettre dans la fiche personnage.
3. Rédiger la fiche personnage
4. La faire relire par le concerné si possible, et plusieurs de ses proches pour compléter s'il y a des oublis notoires.


### Liste des rôles déjà utilisables :
- [Soutien emotionnel](roles/soutien.md)

### Liste des rôles à concevoir :

- Le facilitateur (meneur de jeu / garant du cadre)
- Un co-facilitateur en soutien
- Un citoyen indigné et/ou un gilet jaune
- Nicolas Hulot
- Emmanuel Macron et/ou Poutine et/ou dirigeant USA et/ou dirigeant Chine
- Député(s) et/ou sénateur(s) (de différents partis)
- Un migrant sans papiers ni domicile et/ou une prostituée victime du trafic d'humain
- Une escort girl indépendante
- Un CRS
- Un gars des RG
- Un préfet
- Le président de la BCE, du FMI, de la FED
- Le PDG de Total ou d'Areva
- Le PDG de Bayer/monsanto
- Le PDG de Google
- Un banquier privé (Rodfeiler par exemple)
- Un trader et/ou spéculateur bourgeois ou opportuniste (crypto libertarien)
- Un scientifique du GIEC
- Un économiste orthodoxe
- Un économiste hétérodoxe
- Jancovissi
- Le président de GreenPeace
- Un musulman radicalisé
- Un baron de la drogue
- Un conseiller parlementaire
- Un lobyiste du tabac, du vin, du lait, du bio, du pétrole
- Une mère isolée avec 2 enfants, qui bosse en centre d'appel.
- Un assureur
- Une infirmière
- Un cadre (ou PDG) d'UPS (ou autre compagnie de transport/livraison)
- Un habitant d'éco-lieu techno-sceptique
- Elon Musk
